package com.liislille.companiesapi.controller;

import com.liislille.companiesapi.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/HelloWorld")
public class HelloWorldcontroller {
    @GetMapping("/hello1")
    public String hello1() {
        return "Hello, World (vol 1)";
    }

    @GetMapping("/hello2/{username}")
    public String hello2(@PathVariable String username) {
        return "Hello," + username + "!";

    }
    @GetMapping("/person")
    public Person getPerson() {
        Person teet = new Person ("Teet", "Kask", 5000);
        return teet;
    }
    @GetMapping("/persons")
    public Person[] getRichCustomers() {
        Person maie = new Person ("Maie", "Kask", 6000);
        Person teet = new Person ("Teet", "Kask", 5000);
        Person mai= new Person("Mai","Kask",7000);
        Person[] persons= {
                maie, teet, mai
        };
        return persons;
    }

}

