package com.liislille.companiesapi.controller;

import com.liislille.companiesapi.model.Company;
import com.liislille.companiesapi.repository.CompanyRepository;
import com.liislille.companiesapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")
public class CompanyController {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyService companyService;

    @GetMapping
    public List<Company>getCompanies(){
        return companyRepository.fetchAllCompanies();
    }

    //@GetMapping("/hello2/{username}")
    //public String hello2(@PathVariable String username) {
        //return "Hello," + username + "!";

    @GetMapping("/{companyId}")
    public Company getSingleCompany(@PathVariable int companyId){
        return companyRepository.fetchSingleCompany(companyId);
    }
    @DeleteMapping("/{companyId}")
    public void deleteSingleCompany(@PathVariable int companyId){
        companyRepository.removeSingleCompany(companyId);
    }
    @PostMapping
    public void editCompany(@RequestBody Company company){
        companyService.editCompany(company);

    }
}
