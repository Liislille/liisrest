package com.liislille.companiesapi.service;


import com.liislille.companiesapi.model.Company;
import com.liislille.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public void editCompany(Company company) {
        if (company.getId()>0) {
            companyRepository.updateSingleCompany(company);

        } else {
            companyRepository.addSingleCompany(company);

        }
    }

}
