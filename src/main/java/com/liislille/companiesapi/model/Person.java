package com.liislille.companiesapi.model;

public class Person {
    private String firstName;
    private String lastname;
    private int salary;

    public Person(String firstName, String lastname, int salary) {
        this.firstName = firstName;
        this.lastname = lastname;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
